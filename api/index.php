<?php
require_once "vendor/autoload.php";
require_once "controller.php";
require_once "model.php";

$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false
    ]
];

$c = new \Slim\Container($configuration);
$app = new Slim\App($c);

$c['modelo'] = function () {//Datos de la conexión
    $db = [
        'host' => 'localhost',
        'dbname' => 'centro_excursionista',
        'user' => 'root',
        'password' => ""
    ];


    $datos = new Model($db);
    return $datos;
};

$basicAuth = new Tuupola\Middleware\HttpBasicAuthentication([
    "users" => [
        "user" => "secreto"
    ]
]);

//rutas
$app->get("/socio/", "\Controller:listarSocios")->add($basicAuth); //1. obtener todos los socios
$app->get("/socio/{id}", "\Controller:obtenerSocio")->add($basicAuth);  //2.obtener un socio concreto
$app->get("/seccion/", "\Controller:listarSecciones")->add($basicAuth); //3. obtener todas los secciones
$app->post("/socio/", "\Controller:agregarSocio")->add($basicAuth); //4.nuevo socio
$app->put("/socio/", "\Controller:modificarSocio")->add($basicAuth); //5. modificar datos socio
$app->delete("/socio/{id}", "\Controller:eliminarSocio")->add($basicAuth); //5. eliminar datos socio
$app->get("/seccion/{id}", "\Controller:obtenerSeccion")->add($basicAuth); //6. obtener una seccion concreta
$app->run();

?>